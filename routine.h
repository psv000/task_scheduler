#pragma once

#include <stddef.h>

using routine_exec_t = void (*)(void*);

#define ROUTINE_NOT_STARTED     ( 0 )
#define ROUTINE_IN_PROGRESS     ( 1 << 1 )
#define ROUTINE_FINISHED        ( 1 << 2 )

struct routine_t
{
    routine_exec_t execute;
    void* arguments;
    
    int state;
};
//---------------------------------------------------------------------------------
inline void routine_init(routine_t* routine, routine_exec_t exec, void* exec_args)
{
    routine->execute = exec;
    routine->arguments = exec_args;
    routine->state = ROUTINE_NOT_STARTED;
}
//---------------------------------------------------------------------------------
