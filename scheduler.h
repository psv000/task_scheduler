#pragma once

struct scheduler_attr_t
{
    int routine_pool_length;
};

struct routine_t;
typedef void* scheduler_t;

void scheduler_init(scheduler_t*, const scheduler_attr_t&);
void scheduler_stop_workers(scheduler_t);
void scheduler_deinit(scheduler_t);
void scheduler_add_routine(scheduler_t, routine_t*);

