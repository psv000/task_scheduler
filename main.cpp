#include "scheduler.h"
#include "routine.h"

#include <iostream>

scheduler_t scheduler;

using namespace std;
//---------------------------------------------------------------------------------
void f1(void*)
{
    cout << "a";
}
//---------------------------------------------------------------------------------
void f2(void*)
{
    cout << "b";
}
//---------------------------------------------------------------------------------
void f3(void*);
void fff(void*)
{
    cout << "xxx";
    static int i = 5;
    static routine_t* after_routines = new routine_t[i];
    
    if (i-- > 0)
    {
        routine_init(&(after_routines[i]), (i%2)?f3:f2, NULL);
        scheduler_add_routine(scheduler, &(after_routines[i]));
    }
}
void f3(void*)
{
    static int i = 10;
    static routine_t* after_routines = new routine_t[i];
    
    if (i-- > 0)
    {
        if (i >6)
        {
            routine_init(&(after_routines[i]), fff, NULL);
            scheduler_add_routine(scheduler, &(after_routines[i]));
        }
        else
        {
            routine_init(&(after_routines[i]), f1, NULL);
            scheduler_add_routine(scheduler, &(after_routines[i]));
        }
    }
    
    cout << "c";
}
//---------------------------------------------------------------------------------
int main (int argc, char**argv)
{
    scheduler_attr_t attr;
    attr.routine_pool_length = 400;
    scheduler_init(&scheduler, attr);
    
    const int count = attr.routine_pool_length * 3;
    routine_t routines[count];
    for (int i = 0; i < count; ++i)
    {
        routine_init(&(routines[i]), (i%3 ? f3 : (i%2 ? f2 : f1)), NULL);
        scheduler_add_routine(scheduler, &(routines[i]));
    }
    
    scheduler_stop_workers(scheduler);
    scheduler_deinit(scheduler);
    return 0;
}
//---------------------------------------------------------------------------------
