#include "scheduler.h"
#include "routine.h"

#include <pthread.h>
#include <unistd.h>

#include <stdlib.h>
#include <cassert>
#include <memory>

using thread_descriptor_t = pthread_t;
using mutex_descriptor_t = pthread_mutex_t;
using condition_var_descriptor_t = pthread_cond_t;
using routines_queue_t = routine_t**;

struct scheduler_private_t;

#define SCHEDULER_IS_ALIVE (1 << 1)

#define WORKER_IS_ALIVE  (1 << 1)
#define WORKER_IS_ACTIVE (1 << 2)

#define LOCK(x) pthread_mutex_lock(&x)
#define UNLOCK(x) pthread_mutex_unlock(&x)

struct worker_t
{
    thread_descriptor_t thread_descriptor;
    mutex_descriptor_t mutex;
    condition_var_descriptor_t condition;
    
    scheduler_private_t* scheduler;
    
    routines_queue_t routine_pool;
    _Atomic int routines_pool_usage;
    _Atomic int routines_pool_length;
    _Atomic int flags;
};

struct scheduler_private_t
{
    worker_t* workers;
    int workers_count;
    
    mutex_descriptor_t global_mutex;

    _Atomic int active_worker;
    
    int flags;
};

void* scheduler_start_routines_loop(void*);
inline void scheduler_check_error(scheduler_t, int);

//---------------------------------------------------------------------------------
inline void worker_init(worker_t* self, scheduler_private_t* scheduler, int routine_pool_length)
{
    int err;
    
    self->scheduler = scheduler;
    
    err = pthread_mutex_init(&(self->mutex), NULL);
    scheduler_check_error(self, err);
    
    err = pthread_cond_init(&(self->condition), NULL);
    scheduler_check_error(self, err);
    
    self->flags = WORKER_IS_ALIVE;
    
    pthread_attr_t thread_attr;
    err = pthread_attr_init(&thread_attr);
    scheduler_check_error(self, err);
    
    thread_descriptor_t* thread_id = &(self->thread_descriptor);
    
    err = pthread_create( thread_id, &thread_attr, scheduler_start_routines_loop, (void*)self );
    scheduler_check_error(self, err);
    
    err = pthread_attr_destroy(&thread_attr);
    scheduler_check_error(self, err);
    
    self->routines_pool_length = routine_pool_length;
    self->routines_pool_usage = 0;
    self->routine_pool = (routine_t**)malloc( self->routines_pool_length * sizeof(routine_t*) );
    
    memset( self->routine_pool, 0, self->routines_pool_length * sizeof(routine_t*) );
}
//---------------------------------------------------------------------------------
void scheduler_init(scheduler_t* ptr, const scheduler_attr_t& scheduler_attrs)
{
    int err;
    
    *ptr = malloc(sizeof(scheduler_private_t));
    scheduler_private_t* self = (scheduler_private_t*)*ptr;

    self->flags = SCHEDULER_IS_ALIVE;
    
    int cpu_cores = sysconf(_SC_NPROCESSORS_ONLN);

    self->workers_count = cpu_cores;
    self->workers = (worker_t*)malloc( self->workers_count * sizeof(worker_t) );
    
    err = pthread_mutex_init(&self->global_mutex, NULL);
    scheduler_check_error(self, err);

    for (int i = 0; i < self->workers_count; ++i)
    {
        worker_init( &(self->workers[i]), self, scheduler_attrs.routine_pool_length );
    }
    
    self->active_worker = -1;
}
//---------------------------------------------------------------------------------
void scheduler_stop_workers(scheduler_t ptr)
{
    scheduler_private_t* self = (scheduler_private_t*)ptr;
    self->flags &= ~SCHEDULER_IS_ALIVE;
}
//---------------------------------------------------------------------------------
void scheduler_deinit(scheduler_t ptr)
{
    int err;
    
    scheduler_private_t* self = (scheduler_private_t*)ptr;
    
    for (int i = 0; i < self->workers_count; ++i)
    {
        err = pthread_join(self->workers[i].thread_descriptor, NULL);
        scheduler_check_error(self, err);
        
        err = pthread_mutex_destroy(&(self->workers[i].mutex));
        scheduler_check_error(self, err);
        
        err = pthread_cond_destroy(&(self->workers[i].condition));
        scheduler_check_error(self, err);
        
        assert(self->workers[i].routines_pool_usage == 0);
        
        free(self->workers[i].routine_pool);
    }
    
    err = pthread_mutex_destroy(&self->global_mutex);
    scheduler_check_error(self, err);

    free(self->workers);
    
    self->workers_count = 0;
    self->active_worker = 0;
}
//---------------------------------------------------------------------------------
void scheduler_check_error(scheduler_t self, int error)
{
    assert(error == 0);
}
//---------------------------------------------------------------------------------
void* scheduler_start_routines_loop(void* ptr)
{
    worker_t* worker = (worker_t*)ptr;
    scheduler_private_t* self = worker->scheduler;

    while( worker->flags & WORKER_IS_ALIVE
          || worker->routines_pool_usage > 0 )
    {
        LOCK( worker->mutex );
        
        if ( ( worker->flags & WORKER_IS_ACTIVE ) && ( worker->routines_pool_usage == 0 ) )
        {
            worker->flags &= ~WORKER_IS_ACTIVE;
            while ( !(worker->flags & WORKER_IS_ACTIVE) )
            {
                pthread_cond_wait(&(worker->condition),
                                  &(worker->mutex));
            }
        }
        
        if ( !(self->flags & SCHEDULER_IS_ALIVE) )
        {
            worker->flags &= ~WORKER_IS_ALIVE;
        }
        
        routine_exec_t execute_function = NULL;
        void* arguments = NULL;
        routine_t* routine = NULL;
        
        _Atomic int usage = worker->routines_pool_usage;
        if ( usage > 0 )
        {
            routines_queue_t& current_pool = worker->routine_pool;
            routine = current_pool[0];

            execute_function = routine->execute;
            
            {
                assert(worker->routines_pool_usage > 0);
                routines_queue_t& current_pool = worker->routine_pool;
                assert(routine == current_pool[0]);
                --(worker->routines_pool_usage);
                memmove( current_pool, current_pool + 1, worker->routines_pool_usage * sizeof(routine_t*) );
            }
        }

        UNLOCK( worker->mutex );
        
        if ( execute_function && routine )
        {
            routine->state = ROUTINE_IN_PROGRESS;
            execute_function(arguments);
            routine->state = ROUTINE_FINISHED;
        }
    }
    return nullptr;
}
//---------------------------------------------------------------------------------
void scheduler_add_routine(scheduler_t ptr, routine_t* routine)
{
    scheduler_private_t* self = (scheduler_private_t*)ptr;
    
    self->active_worker = ++self->active_worker % self->workers_count;
    _Atomic int active_worker = self->active_worker;
    
    LOCK( self->workers[self->active_worker].mutex );
    {
        worker_t* worker = &(self->workers[self->active_worker]);
        
        assert(worker->routines_pool_usage < worker->routines_pool_length);
        
        routines_queue_t& current_pool = worker->routine_pool;
        int pool_usage = worker->routines_pool_usage;
        
        current_pool[pool_usage] = routine;
        routine->state = ROUTINE_NOT_STARTED;
        
        if ( !(worker->flags & WORKER_IS_ACTIVE) )
        {
            worker->flags |= WORKER_IS_ACTIVE;
            pthread_cond_broadcast(&(worker->condition));
        }
        
        ++(worker->routines_pool_usage);
        
    }
    UNLOCK( self->workers[active_worker].mutex );
}
//---------------------------------------------------------------------------------
